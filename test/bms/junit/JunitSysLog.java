package bms.junit;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pers.yaoliguo.bms.dao.SysLogDao;
import pers.yaoliguo.bms.entity.SysLog;
import pers.yaoliguo.bms.service.ISysLogService;
import pers.yaoliguo.bms.uitl.StringHelper;

/**
 * @ClassName:       JunitSysLog
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年6月5日        下午8:43:24
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring_bean.xml"})  
public class JunitSysLog {
	
	@Autowired
	ISysLogService sysLogService;
	
	
	
	@Test
	public void addLog(){
		sysLogService.recordLog(StringHelper.getUUID(), "root", "junitTest", "add");
		
	}
	
	@Test
	public void getById(){
		SysLog log = sysLogService.getById("b0fc587cce63408288b6c86cf0ddb66e");
		System.out.println(log.getMoudle()+" "+log.getOperation());
	}
	
	@Test 
	public void getAllLog(){
		List<Map> list = sysLogService.getAllLog();
		System.out.println("++++++++++++++++++++++");
		for (Map map : list) {
			System.out.println(map);
			System.out.println(map.get("moudle"));
		}
	}

}
