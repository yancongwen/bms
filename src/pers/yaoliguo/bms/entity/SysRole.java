package pers.yaoliguo.bms.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SysRole implements Serializable{
    private String id;

    private String roleName;

    private String pid;

    private String descript;

    private Boolean del;

    private List<SysRole> children = new ArrayList<SysRole>();
    public String getId() {
        return id;
    }

    public List<SysRole> getChildren() {
		return children;
	}

	public void setChildren(List<SysRole> children) {
		this.children = children;
	}

	public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName == null ? null : roleName.trim();
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript == null ? null : descript.trim();
    }

    public Boolean getDel() {
        return del;
    }

    public void setDel(Boolean del) {
        this.del = del;
    }
}