package pers.yaoliguo.bms.listener;

import java.util.HashMap;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import pers.yaoliguo.bms.entity.SysUser;
import pers.yaoliguo.bms.uitl.Global;

public class OnlineUsers implements HttpSessionAttributeListener {

	HashMap<String, OnlineUser> users = new HashMap<String, OnlineUser>();

	@Override
	public void attributeAdded(HttpSessionBindingEvent event) {

		HttpSession session = event.getSession();
		SysUser user = (SysUser) session.getAttribute(Global.LOGIN_USER_KEY);
		// 如果session中user不为空，说明已经登录成功
		synchronized (users) {
			if (user != null) {
				// 查看users中是否有该用户
				OnlineUser onlieUser = users.get(user.getAccount());

				if (onlieUser != null) {

					// 如果同一sessionId不相同，则说明在其他地方登录
					if (!onlieUser.getSession().getId().equals(session.getId())) {
						users.remove(user.getAccount());
						onlieUser.getSession().invalidate();
						System.out.println(user.getAccount()
								+ "已被挤下线了------------------------");
						onlieUser = new OnlineUser();
						onlieUser.setSession(session);
						onlieUser.setSessionId(session.getId());
						onlieUser.setUser(user);
						users.put(user.getAccount(), onlieUser);
						System.out.println(user.getAccount()
								+ "上线了------------------------");
					}

				}else{
					onlieUser = new OnlineUser();
					onlieUser.setSession(session);
					onlieUser.setSessionId(session.getId());
					onlieUser.setUser(user);
					users.put(user.getAccount(), onlieUser);
					System.out.println(user.getAccount()
							+ "上线了------------------------");
				}

			}
		}

	}

	@Override
	public void attributeRemoved(HttpSessionBindingEvent event) {

	}

	@Override
	public void attributeReplaced(HttpSessionBindingEvent event) {

	}

	/**
	 * 对象实例(即OnlineUserListener的实例)作为一个属性被设置到session的
	 * 时候，会调用本方法，这种情况一般发生在点击登录按钮以后的处理过程中 设置
	 * 
	 */
	/*
	 * @Override public void valueBound(HttpSessionBindingEvent event) {
	 * 
	 * HttpSession session = event.getSession(); SysUser user = (SysUser)
	 * session.getAttribute(Global.LOGIN_USER_KEY); //如果session中user不为空，说明已经登录成功
	 * synchronized (users) { if(user != null){ //查看users中是否有该用户 OnlineUser
	 * onlieUser = users.get(user.getAccount());
	 * 
	 * if(onlieUser != null){
	 * 
	 * //如果同一sessionId不相同，则说明在其他地方登录
	 * if(!onlieUser.getSession().getId().equals(session.getId())){
	 * onlieUser.getSession().invalidate();
	 * System.out.println(user.getAccount()+"已被挤下线了------------------------"); }
	 * 
	 * }else{ onlieUser = new OnlineUser(); onlieUser.setSession(session);
	 * onlieUser.setSessionId(session.getId()); onlieUser.setUser(user);
	 * users.put(user.getAccount(), onlieUser);
	 * System.out.println(user.getAccount()+"上线了------------------------"); }
	 * 
	 * } }
	 * 
	 * }
	 */

	/**
	 * 当Session超时，或本实例被从session中移除的时候被调用，这种情况一般 发生在注销方法的处理过程中
	 */
	/*
	 * @Override public void valueUnbound(HttpSessionBindingEvent event) {
	 * 
	 * }
	 */

}
