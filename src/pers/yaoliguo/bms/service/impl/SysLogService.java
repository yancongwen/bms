package pers.yaoliguo.bms.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pers.yaoliguo.bms.dao.SysLogDao;
import pers.yaoliguo.bms.entity.SysLog;
import pers.yaoliguo.bms.service.ISysLogService;
import pers.yaoliguo.bms.uitl.StringHelper;

/**
 * @ClassName:       SysLogService
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年6月5日        下午8:33:08
 */
@Service("sysLogService")
@Transactional
public class SysLogService implements ISysLogService {
	
	@Autowired
	SysLogDao sysLogDao;

	
	@Override
	public int recordLog(SysLog log) {
		
		return sysLogDao.recordLog(log);
	}

	
	@Override
	public SysLog getById(String id) {

		return sysLogDao.getById(id);
	}

	
	@Override
	public int recordLog(String userId, String userName, String moudle,
			String operation) {
		
		SysLog log = new SysLog();
		log.setCreateTime(new Date());
		log.setId(StringHelper.getUUID());
		log.setMoudle(moudle);
		log.setOperation(operation);
		log.setUserId(userId);
		log.setUserName(userName);
		
		return recordLog(log);
	}


	@Override
	public List<Map> getAllLog() {
		
		return sysLogDao.getAllLog();
	}

	
	
}
