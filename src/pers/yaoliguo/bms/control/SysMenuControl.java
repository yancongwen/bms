package pers.yaoliguo.bms.control;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import pers.yaoliguo.bms.control.view.MenuView;
import pers.yaoliguo.bms.control.view.Message;
import pers.yaoliguo.bms.entity.SysMenu;
import pers.yaoliguo.bms.entity.SysUser;
import pers.yaoliguo.bms.service.impl.SysMenuService;
import pers.yaoliguo.bms.uitl.EhcacheUtil;
import pers.yaoliguo.bms.uitl.PermissionCodeUtil;
import pers.yaoliguo.bms.uitl.StringHelper;

/**
 * @ClassName:       SysMenuControl
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年7月1日        下午3:13:44
 */
@Controller
@RequestMapping(value="/SysMenuControl")
public class SysMenuControl extends BaseControl{

	@Autowired
	SysMenuService sysMenuService;
	
	//@RequiresPermissions(value=PermissionCodeUtil.MENU)
	@RequestMapping(value="/skipMenuPage")
	public String skipMenuPage(){
		
		return "redirect:/views/menu/menuList.html";
	}
	
	//@RequiresPermissions(value=PermissionCodeUtil.MENU)
	@RequestMapping(value="/getMenuList")
	@ResponseBody
	public Object getMenuList(MenuView menuView){
		
		Message<SysMenu> msg = new Message<SysMenu>();
		
		if(menuView ==  null){
			menuView = new MenuView();
			menuView.setPid("-1");
		}else{
			if(menuView.getPid() == null || menuView.getPid().equals("")){
				menuView.setPid("-1");
			}
		}
		menuView.setDel(false);
		List<SysMenu> list = null;
		try {
			
			list = sysMenuService.selectAll(menuView);
			msg.setResult("200");
			
		} catch (Exception e) {
			msg.setResult("500");
			msg.setInfo(e.getMessage());
		}
		
		msg.setDataList(list);
		
		
		return msg;
	}
	
	@RequiresUser
	@RequestMapping(value="/getcurrentUserMenu")
	@ResponseBody
	public Object getcurrentUserMenu(){
		Message<SysMenu> msg = new Message<SysMenu>();
		SysUser u = getLoginUser();
		List<SysMenu> list = (java.util.List<SysMenu>) EhcacheUtil.getInstance().get(EhcacheUtil.MENU_CACHE, u.getAccount());
		List<String> ids = new ArrayList<String>();
		SysMenu sysmenu = new SysMenu();
		sysmenu.setPid("-1");
		//查出所有pid为-1的菜单
		List<SysMenu> allMenu = sysMenuService.selectAll(sysmenu);
		//将菜单的id赋给list
		for(SysMenu sys:list){
			ids.add(sys.getId());
		}
		//删除没用的menu
		for(int i = 0; i < allMenu.size();i++){
			String t = deleteUnuseMenu(allMenu.get(i),ids);
			if(t == null){
				 allMenu.remove(allMenu.get(i));
				 i--;
			 }
			
		}
		 
		msg.setDataList(allMenu);
		msg.setResult("200");
		return msg;
	}
	 
	
	public String deleteUnuseMenu(SysMenu sysMenu, List<String> ids){
		 String menuID = null;
		 if(ids.contains(sysMenu.getId())){
			 menuID = sysMenu.getId();
		 }
		 else if(sysMenu.getChildren().size()>0){
			 for(int i = 0; i < sysMenu.getChildren().size();i++){
				 String t = deleteUnuseMenu(sysMenu.getChildren().get(i),ids);
				 if(t == null){
					 sysMenu.getChildren().remove(sysMenu.getChildren().get(i));
					 i--;
				 }
				 else{
					 menuID = t;
				 }
				
			 }
		}
		 return menuID;
	}
	@RequiresPermissions(value=PermissionCodeUtil.MENU)
	@RequestMapping(value="/addMenu")
	@ResponseBody
	public Object addMenu(MenuView menuView){
		
		Message<MenuView> msg = new Message<MenuView>();
		
		if(menuView ==  null){
			msg.setResult("400");
			msg.setInfo("不能为空");
		}else{
			menuView.setId(StringHelper.getUUID());
			menuView.setDel(false);
			sysMenuService.insert(menuView);
			msg.setResult("200");
			msg.setInfo("添加成功!");
		}
		
		return msg;
	}
	
	@RequiresPermissions(value=PermissionCodeUtil.MENU)
	@RequestMapping(value="/updateMenu")
	@ResponseBody
	public Object updateMenu(MenuView menuView){
		
		Message<MenuView> msg = new Message<MenuView>();
		
		if(menuView ==  null){
			msg.setResult("400");
			msg.setInfo("不能为空");
		}else{
			sysMenuService.updateByPrimaryKey(menuView);
			msg.setResult("200");
			msg.setInfo("修改成功!");
		}
		
		return msg;
	}
	
	@RequiresPermissions(value=PermissionCodeUtil.MENU)
	@RequestMapping(value="deleteMenu")
	@ResponseBody
	public Object deleteMenu(MenuView menuView){
		
		Message<MenuView> msg = new Message<MenuView>();
		
		if(menuView ==  null){
			msg.setResult("400");
			msg.setInfo("不能为空");
		}else{
			if(StringHelper.isNullOrEmpty(menuView.getPid())){
				menuView.setPid("-1");
			}
			menuView.setDel(true);
			sysMenuService.updateByPrimaryKeySelective(menuView);
			sysMenuService.removeChildren(menuView);
			msg.setResult("200");
			msg.setInfo("修改成功!");
		}
		
		return msg;
	}
	
	
}
