package pers.yaoliguo.bms.control;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import pers.yaoliguo.bms.control.view.Message;
import pers.yaoliguo.bms.entity.SysOrganiz;
import pers.yaoliguo.bms.service.ISysOrganizService;
import pers.yaoliguo.bms.uitl.StringHelper;
import pers.yaoliguo.bms.uitl.PermissionCodeUtil;
/**
 * @ClassName:       SysOrganizControl
 * @Description:    TODO
 * @author:            wangyi
 * @date:            2017年7月28日14:02:11
 */
@Controller
@RequestMapping("/SysOrgControl")
public class SysOrganizeControl extends BaseControl{
	
	@Autowired
	ISysOrganizService organizService;
	
	//@RequiresPermissions(value=PermissionCodeUtil.Organize)
	@RequestMapping(value="/skiporganizPage")
	public String skipOrganizePage(){
		
		return "redirect:/views/organiz/organizList.html";
	}
	
	//@RequiresPermissions(value=PermissionCodeUtil.Organize)
	@RequestMapping("/getOrganizList")
	@ResponseBody
	public Object getOrganizeList(SysOrganiz sysOrganiz){
		Message<SysOrganiz> msg = new  Message<SysOrganiz>();
		try {
		List<SysOrganiz>sysOrganizList	= organizService.selectOrganizsByKey(sysOrganiz); 
		msg.setDataList(sysOrganizList);
		msg.setResult("200");
		} catch (Exception e) {
			// TODO: handle exception
			msg.setResult("500");
		}
		return msg;
	}
	
	//@RequiresPermissions(value=PermissionCodeUtil.Organize)
	@RequestMapping("/addOrganiz")
	@ResponseBody
	public Object addOrganiz(SysOrganiz sysOrganiz){
		
		Message<SysOrganiz> msg = new  Message<SysOrganiz>();
		
		if(sysOrganiz == null || StringHelper.isNullOrEmpty(sysOrganiz.getOrganizName())){
			msg.setResult("400");
			msg.setInfo("不能为空");
		}else{
			
			if(StringHelper.isNullOrEmpty(sysOrganiz.getPid())){
				sysOrganiz.setPid("-1");
			}
			try {
				sysOrganiz.setId(StringHelper.getUUID());
				organizService.insert(sysOrganiz);
				msg.setResult("200");
				msg.setInfo("添加成功!");
			} catch (Exception e) {
				// TODO: handle exception
				msg.setResult("500");
				msg.setInfo(e.getMessage());
			} 
			
			
		}
		
		return msg;
	}
	
	//@RequiresPermissions(value=PermissionCodeUtil.Organize)
	@RequestMapping("/updateOrganiz")
	@ResponseBody
	public Object updateOrganize(SysOrganiz sysOrganiz){
		Message<SysOrganiz> msg = new  Message<SysOrganiz>();
		
		if(sysOrganiz == null || StringHelper.isNullOrEmpty(sysOrganiz.getOrganizName())){
			
			msg.setResult("400");
			msg.setInfo("不能为空");
		}else{
			try {
				organizService.updateByPrimaryKey(sysOrganiz); 
				msg.setResult("200");
				msg.setInfo("修改成功!");
			} catch (Exception e) {
				// TODO: handle exception
				msg.setResult("500");
				msg.setInfo(e.getMessage());
			}
			
		}
		
		return msg;
	}
	
	//@RequiresPermissions(value=PermissionCodeUtil.Organize)
	@RequestMapping("/deleteOrganiz")
	@ResponseBody
	public Object deleteOrganize(SysOrganiz sysOrganiz){
		Message<SysOrganiz> msg = new  Message<SysOrganiz>();
		
		if(sysOrganiz == null){
			msg.setResult("400");
			msg.setInfo("不能为空");
		}else{
			if(StringHelper.isNullOrEmpty(sysOrganiz.getPid())){
				sysOrganiz.setPid("-1");
			}
			
			sysOrganiz.setDel(true);
			organizService.updateByPrimaryKeySelective(sysOrganiz);
			organizService.removeChildren(sysOrganiz);
			msg.setResult("200");
			msg.setInfo("删除成功!");
		}
		
		return msg;
	}

}
