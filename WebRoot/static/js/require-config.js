 
	(function(window){var WebRootUrl = function(){
		 
		// http://localhost:8080/BMS/views/login.html;jsessionid=9A918AE98683A1B8C207FAF6959A9E9D
		var curWwwPath=window.document.location.href;
		
		// /BMS/views/login.html;jsessionid=9A918AE98683A1B8C207FAF6959A9E9D
	    var pathName=window.document.location.pathname;
	   
	    var pos=curWwwPath.indexOf(pathName);
	    // http://localhost:8080
	    var localhostPaht=curWwwPath.substring(0,pos);  
	    // BMS
	    var projectName=pathName.substring(0,pathName.substr(1).indexOf('/')+1);
	    
	    return localhostPaht+projectName;
	}
	require.config({
        baseUrl:WebRootUrl()+'/static/js',
	     paths: {
	     "ELEMENT":"elementUI/index",
	     'vue':'lib/vue',
	     'velocity':'lib/velocity.min',
	     'vue-resource':'lib/vue-resource',
	     'index':'index/index',
	     'base':'base',
	     'Message':'vueplus/message',
	     'Scroll':'vueplus/scroll'
	    }
	});
	})(window);
